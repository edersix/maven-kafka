package com.github.edersix.kafka.tutotial1;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

public class ProducerDemo {
	public static void main(String[] args) {
//		System.out.println("hello eder");
		String bootstrapServers="127.0.0.1:9092";
		
		
//		create producer properties
//		https://kafka.apache.org/documentation/#producerconfigs
		Properties properties = new Properties();
//		properties.setProperty("bootstrap.servers", bootstrapServers);
//		properties.setProperty("key.serializer", StringSerializer.class.getName());
//		properties.setProperty("value.serializer", StringSerializer.class.getName());

		properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		
		
//		crate a producer in this case String, String to set the key and value as string
		KafkaProducer<String, String> producer = new KafkaProducer<String, String>(properties);
		
//		create a producer record  topicName  and message 
		ProducerRecord<String, String> record = 
				new ProducerRecord<String, String>("diego-events", "hello world");
		
//		send data
		producer.send(record);
//		flush data
		producer.flush();
//		flush and close producer
		producer.close();
	}
}
