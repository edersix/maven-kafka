package com.github.edersix.kafka.tutotial1;

import java.util.Properties;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProducerDemoWithKey {
static Logger logger = LoggerFactory.getLogger(ProducerDemoWithKey.class);
	
	public static void main(String[] args) throws InterruptedException, ExecutionException {
//		System.out.println("hello eder");
		String bootstrapServers="127.0.0.1:9092";
		
		
//		create producer properties
//		https://kafka.apache.org/documentation/#producerconfigs
		Properties properties = new Properties();
//		properties.setProperty("bootstrap.servers", bootstrapServers);
//		properties.setProperty("key.serializer", StringSerializer.class.getName());
//		properties.setProperty("value.serializer", StringSerializer.class.getName());

		properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		
		
		//create safe producer

		properties.setProperty(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, "true");
		properties.setProperty(ProducerConfig.ACKS_CONFIG, "all");
		properties.setProperty(ProducerConfig.RETRIES_CONFIG, Integer.toString(Integer.MAX_VALUE));
		properties.setProperty(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION, "5");
		
//		increase performance-------------------------------		
//		message compression snappy a a good option to compress json cause is tex based messages
		properties.setProperty(ProducerConfig.COMPRESSION_TYPE_CONFIG, "snappy");//none, gzip, lz4, snappy
		
//		linger to wait messages to include in batch
		properties.setProperty(ProducerConfig.LINGER_MS_CONFIG, "5");//milliseconds
		properties.setProperty(ProducerConfig.BATCH_SIZE_CONFIG, Integer.toString(23*1024));//32KB
//		increase performance-------------------------------		
		
//		crate a producer in this case String, String to set the key and value as string
		KafkaProducer<String, String> producer = new KafkaProducer<String, String>(properties);
		
//		create a producer record  topicName  and message 
		ProducerRecord<String, String> record;
		
//		send data
		String topic="diego-events";
		String value;
		String key;
		for (int i = 0; i < 10; i++) {
			value="hello world"+String.valueOf(i);
			key="Key_"+Integer.toString(i);
					
			record = new ProducerRecord<String, String>(topic, key, value);
			producer.send(record, new Callback() {
				
				public void onCompletion(RecordMetadata metadata, Exception e) {
//					Logger logger = LoggerFactory.getLogger(ProducerDemoWithKey.class.getEnclosingMethod().getName());
					// Executes every time  a record is successfully sent or an exception is thrown
					if (e==null) {
						//the record was successfully sent
						logger.info("received new metadata \n"+
								"Topic:"+metadata.topic()+"\n"+
								"Partition:"+metadata.partition()+"\n"+
								"Offset:"+metadata.offset()+"\n"+
								"Timestamp:"+metadata.timestamp()+"\n");
					} else {
						logger.error("Error while sending data: "+e);
					}
					
				}
			});//.get();// block the .sent to make it synchronous don't do this in production
		}
//		flush data
		producer.flush();
//		flush and close producer
		producer.close();
	}
}
