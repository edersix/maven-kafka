package com.github.edersix.kafka.tutotial1;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConsumerDemoWithThread {

	
	public static void main(String[] args) {
		
		
		new ConsumerDemoWithThread().run();
		
//		create a consumer
//		KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(properties);
;		
//		subscribe consumer to our topic
//		consumer.subscribe(Collections.singleton(topic));// only subscribe to one topic
//		consumer.subscribe(Arrays.asList(topic,"topic-n"));// subscribe to more than one topic
		
//		poll for new data
		
		
	}
	
	
	private ConsumerDemoWithThread() {
		
	}
	
	private void run() {
		final Logger logger = LoggerFactory.getLogger(ConsumerDemoWithThread.class.getName());
		String bootstrapServers="127.0.0.1:9092";
		String groupId="myAppConsumerDemo3";
		String topic="diego-events";
		
		//latch for dealing with multiple threads
		CountDownLatch latch= new CountDownLatch(1);
		
		
		//create a consumer runnable
		logger.info("Creating the consumer thread");
		final Runnable myConsumerRunnable = 
				new ConsumerRunnable(bootstrapServers, groupId, topic, latch);
		
		Thread myThread = new Thread(myConsumerRunnable);
		myThread.start();
		
		
		//add a shut down hook
		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			logger.info("Application is closing");
			((ConsumerRunnable) myConsumerRunnable).shutdown();
		}
		));
		
		try {
			latch.await();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			logger.info("Application is closing");
		}
	}
	
	public class ConsumerRunnable implements Runnable{
		private Logger logger = LoggerFactory.getLogger(ConsumerRunnable.class.getName());
		private CountDownLatch latch;
		private KafkaConsumer<String, String> consumer; 
		
		public ConsumerRunnable(String bootstrapServers,
							String groupId,
							String topic,
							CountDownLatch latch) {
			Properties properties = new Properties();
			
//			create a cnsumer config
			//set consumer properties using consumerConfig
			properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
			properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
			properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
			properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId);
			properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");//none or latest
			
			this.latch=latch;
			this.consumer = new KafkaConsumer<String, String>(properties);
			
			consumer.subscribe(Arrays.asList(topic));
		}


		public void run() {
			// TODO Auto-generated method stub
			try {
			
				while (true) {
	//				consumer.poll(100);//deprecated
					ConsumerRecords<String, String> records = 
							consumer.poll(Duration.ofMillis(100));// new way in Kafka 2.0.0  CTRL+2 to assign to a new local variable
					for (ConsumerRecord<String, String> record : records) {
						logger.info("received new metadata \n"+
								"Key:"+record.key()+"\n"+
								"Topic:"+record.topic()+"\n"+
								"Partition:"+record.partition()+"\n"+
								"Offset:"+record.offset()+"\n"+
								"Value:"+record.value()+"\n");
					}
					
				}	
				
			} catch (WakeupException we) {
				logger.info("receive shut down signal!");
			}
			finally {
				consumer.close();
				latch.countDown();
			}
		}
		
		public void shutdown()
		{
			//the wakeuo methos is a special method to interrup consumer.poll
			//it will trhow the exception wakeupexception
			consumer.wakeup();
		}
	}
}
