package com.github.edersix.kafka.tutotial1;

import java.time.Duration;
import java.util.Arrays;
import java.util.Collections;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConsumerDemoAssignSeek {
static Logger logger = LoggerFactory.getLogger(ConsumerDemoAssignSeek.class.getName());
	
	public static void main(String[] args) {

		String bootstrapServers="127.0.0.1:9092";
//		String groupId="myAppConsumerDemo4";
		String topic="diego-events";
		
		Properties properties = new Properties();
		
//		create a cnsumer config
		//set consumer properties using consumerConfig
		properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
		properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
//		properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId);
		properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");//none or latest
		
//		create a consumer
		KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(properties);

// 		Assign and seek are mostly used  to reply data or fetch a specific message

//		Assign
		TopicPartition partitionToReadFrom = new TopicPartition(topic, 0);
		long offsetToReadFrom =15L;// L is for long
		consumer.assign(Arrays.asList(partitionToReadFrom));
		
//		Seek
		consumer.seek(partitionToReadFrom, offsetToReadFrom);
		
		
		int numberOfMessagesToRead = 5;
		boolean keepOnReading= true;
		int numberOfMessagesReadSoFar=0;
		

//		poll for new data
		while (keepOnReading) {
//			consumer.poll(100);//deprecated
			ConsumerRecords<String, String> records = 
					consumer.poll(Duration.ofMillis(100));
			// new way in Kafka 2.0.0  CTRL+2 to assign to a new local variable
			for (ConsumerRecord<String, String> record : records) {
				numberOfMessagesReadSoFar++;
				logger.info("received new metadata \n"+
						"Key:"+record.key()+"\n"+
						"Topic:"+record.topic()+"\n"+
						"Partition:"+record.partition()+"\n"+
						"Offset:"+record.offset()+"\n"+
						"Value:"+record.value()+"\n");
				if(numberOfMessagesReadSoFar >= numberOfMessagesToRead) {
					keepOnReading=false;
					break;
				}
			}
			
			logger.info("exiting the application");
			
		}	
		consumer.close();
	}
}
