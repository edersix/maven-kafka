package com.github.edersix.kafka.tutotial3;

import java.io.IOException;
import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonParser;

public class ElasticSearchConsumer {
	
	public static RestHighLevelClient createClient() {
		String hotstname="";
		String username="";
		String password="";
		
//		Don't do if you run a local ElasticSearch only needed if you use a cloud elastic search
		final CredentialsProvider credentialProvider = new BasicCredentialsProvider();
		credentialProvider.setCredentials(AuthScope.ANY,
				new UsernamePasswordCredentials(username,password));
		
		RestClientBuilder builder = RestClient.builder(
				new HttpHost(hotstname, 443, "https"))
				.setHttpClientConfigCallback(new RestClientBuilder.
						HttpClientConfigCallback() {

					@Override
					public HttpAsyncClientBuilder customizeHttpClient
					(HttpAsyncClientBuilder httpClientBuilder) {
						// TODO Auto-generated method stub
						return httpClientBuilder.setDefaultCredentialsProvider
								(credentialProvider);
					}
				
				});
				
		RestHighLevelClient client = new RestHighLevelClient(builder);
		return client;
				
	}
	
	public static KafkaConsumer<String,String> createConsumer(String topic){
		String bootstrapServers="127.0.0.1:9092";
		String groupId="myAppConsumerDemo2";
//		String topic="diego-events";
		
		Properties properties = new Properties();
		
//		create a cnsumer config
		//set consumer properties using consumerConfig
		properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, 
				StringDeserializer.class.getName());
		properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, 
				StringDeserializer.class.getName());
		properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId);
		properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");//none or latest

		properties.setProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");//manual commit of offsets
		properties.setProperty(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, "10");//manual commit of offsets
		
//		create a consumer
		KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(properties);
;		
//		subscribe consumer to our topic
		consumer.subscribe(Collections.singleton(topic));// only subscribe to one topic
		return consumer;
	}
	
	
	private static String extractIdFromTweet(String json) {
		return JsonParser.parseString(json).getAsJsonObject().get("id_str").getAsString();
	}
	
	public static void main(String[] args) throws IOException {
		Logger logger = LoggerFactory.getLogger(ElasticSearchConsumer.class.getName());
		
		RestHighLevelClient client = createClient();
		
//		String jsonString = "{\"foo\":\"bar\"}";
		
//		@SuppressWarnings("deprecation")
//		IndexRequest indexRequest= new IndexRequest("twitter","teets")
//				.source(jsonString, XContentType.JSON);
		
//		IndexResponse indexResponse = client.index(indexRequest, RequestOptions.DEFAULT);
//		String id= indexResponse.getId();
//		logger.info(id);
		
		KafkaConsumer<String, String> consumer= createConsumer("twitter_teets");
		
		while (true) {
//			consumer.poll(100);//deprecated
			ConsumerRecords<String, String> records = 
					consumer.poll(Duration.ofMillis(100));// new way in Kafka 2.0.0  
			
			logger.info("Received "+records.count()+"records");
			
			
//			a way to send a group of request 001
//			BulkRequest bulkRequest= new BulkRequest();
			
			for (ConsumerRecord<String, String> record : records) {
//				2 strategies
//				kafka generic ID
//				String id= record.topic()+"_"+record.partition()+"_"+record.offset();
				
//				same as record
				 String id=extractIdFromTweet(record.value());
				
//				where we insert data into ElasticSearch
				@SuppressWarnings("deprecation")
				IndexRequest indexRequest= new IndexRequest(
						"twitter",//index
						"teets",//tweets
						id)//this is to make our consumer idempotent
						.source(record.value(), XContentType.JSON);
//				001
//				bulkRequest.add(indexRequest);
				
				IndexResponse indexResponse = client.index(indexRequest, RequestOptions.DEFAULT);
				id= indexResponse.getId();
				logger.info(id);
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
//			001
//			BulkResponse bulkResponses = client.bulk(bulkRequest, RequestOptions.DEFAULT);
			
			logger.info("Commiting offsets");
			consumer.commitSync();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			};
		}			
		
//		client.close();
		
		
		
	}

}
