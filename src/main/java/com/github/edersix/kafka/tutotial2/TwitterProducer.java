package com.github.edersix.kafka.tutotial2;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import com.twitter.hbc.ClientBuilder;
import com.twitter.hbc.core.Client;
import com.twitter.hbc.core.Constants;
import com.twitter.hbc.core.Hosts;
import com.twitter.hbc.core.HttpHosts;
import com.twitter.hbc.core.endpoint.StatusesFilterEndpoint;
import com.twitter.hbc.core.processor.StringDelimitedProcessor;
import com.twitter.hbc.httpclient.auth.Authentication;
import com.twitter.hbc.httpclient.auth.OAuth1;

public class TwitterProducer {
	
	
	Logger logger = LoggerFactory.getLogger(TwitterProducer.class);
	
	private String consumerKey="";
	private String consumerSecret="";
	private String token="";
	private String secret="";
	
	public TwitterProducer() {
		
	}
	
	public static void main(String[] args) {
		
	}
	
	
	public void run() {
		
		/** Set up your blocking queues: Be sure to size these properly based on expected TPS of your stream */
		BlockingQueue<String> msgQueue = new LinkedBlockingQueue<String>(1000);

		//create a twitter client
		Client client = createTwitterClient(msgQueue);
		//		attempts to establish a connection
		client.connect();
		
		//create a kafka producer
		KafkaProducer<String, String> producer = createKafkaProducer();
		
		//		loop to send twetts to kafka
		// on a different thread, or multiple different threads....
		while (!client.isDone()) {
			String msg=null;
			try {
				msg = msgQueue.poll(5,TimeUnit.SECONDS);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				client.stop();
			}
			if(msg!=null) {
				logger.info("message="+msg);
				producer.send(new ProducerRecord<String, String>("twitter_tweets", null,msg),new Callback() {
					
					@Override
					public void onCompletion(RecordMetadata metadata, Exception e) {
						// TODO Auto-generated method stub
						if (e==null) {
							//the record was successfully sent
							logger.info("received new metadata \n"+
									"Topic:"+metadata.topic()+"\n"+
									"Partition:"+metadata.partition()+"\n"+
									"Offset:"+metadata.offset()+"\n"+
									"Timestamp:"+metadata.timestamp()+"\n");
						} else {
							logger.error("Error while sending data: "+e);
						}
					}
				});
			}
			//		  something(msg);
			//		  profit();
		}
	}

	
	
	public Client createTwitterClient(BlockingQueue<String> msgQueue) {
//		move to receive as parameter
//		/** Set up your blocking queues: Be sure to size these properly based on expected TPS of your stream */
//		BlockingQueue<String> msgQueue = new LinkedBlockingQueue<String>(100000);
		
		/** Declare the host you want to connect to, the endpoint, and authentication (basic auth or oauth) */
		Hosts hosebirdHosts = new HttpHosts(Constants.STREAM_HOST);
		StatusesFilterEndpoint hosebirdEndpoint = new StatusesFilterEndpoint();
		// Optional: set up some followings and track terms
//		List<Long> followings = Lists.newArrayList(1234L, 566788L);
		List<String> terms = Lists.newArrayList("bitcoin");//word to be getter
//		hosebirdEndpoint.followings(followings);
		hosebirdEndpoint.trackTerms(terms);

		// These secrets should be read from a config file
		Authentication hosebirdAuth = new OAuth1(consumerKey, consumerSecret, token,secret);
		
		
		ClientBuilder builder = new ClientBuilder()
				.name("Hosebird-Client-01")                              // optional: mainly for the logs
				.hosts(hosebirdHosts)
				.authentication(hosebirdAuth)
				.endpoint(hosebirdEndpoint)
				.processor(new StringDelimitedProcessor(msgQueue));
		//				  .eventMessageQueue(eventQueue);                          // optional: use this if you want to process client events

		Client hosebirdClient = builder.build();
		// Attempts to establish a connection.
//		hosebirdClient.connect();
		return hosebirdClient;

	}
	
	public KafkaProducer<String , String> createKafkaProducer(){
	String bootstrapServers="127.0.0.1:9092";
		
//		create producer properties
//		https://kafka.apache.org/documentation/#producerconfigs
		Properties properties = new Properties();
		properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		
		
		//create safe producer

		properties.setProperty(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, "true");
		properties.setProperty(ProducerConfig.ACKS_CONFIG, "all");
		properties.setProperty(ProducerConfig.RETRIES_CONFIG, Integer.toString(Integer.MAX_VALUE));
		properties.setProperty(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION, "5");
		
//		increase performance-------------------------------		
//		message compression snappy a a good option to compress json cause is text based messages
		properties.setProperty(ProducerConfig.COMPRESSION_TYPE_CONFIG, "snappy");//none, gzip, lz4, snappy
		
//		linger to wait messages to include in batch
		properties.setProperty(ProducerConfig.LINGER_MS_CONFIG, "5");//milliseconds
		properties.setProperty(ProducerConfig.BATCH_SIZE_CONFIG, Integer.toString(23*1024));//kilobytes 32KB
		
//		crate a producer in this case String, String to set the key and value as string
		KafkaProducer<String, String> producer = new KafkaProducer<String, String>(properties);
		
		return producer;
	}

}
