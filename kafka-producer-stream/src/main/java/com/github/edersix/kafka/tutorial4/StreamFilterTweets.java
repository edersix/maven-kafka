package com.github.edersix.kafka.tutorial4;

import java.util.Properties;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;

import com.google.gson.JsonParser;

public class StreamFilterTweets {
	public static void main(String[] args) {
//		create properties
		Properties properties = new Properties();
		properties.setProperty(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
		properties.setProperty(StreamsConfig.APPLICATION_ID_CONFIG, "demo-kafka-streams");
		properties.setProperty(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.StringSerde.class.getName());
		properties.setProperty(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.StringSerde.class.getName());
		
//		create topology
		StreamsBuilder streamBuilder= new StreamsBuilder();
		
//		input topic
		KStream<String, String> inputTopic = streamBuilder.stream("twetter_Tweets");
		KStream<String, String> filteredStream = inputTopic.filter(
//				fulter tweets  which has a user of over 10000 followers
				(k, jsonTweet) ->  extractUserFolowerInTweet(jsonTweet)>10000
			);
		filteredStream.to("important_tweets");
		
//		build the topology
		KafkaStreams kafkaStreams = new KafkaStreams(
				streamBuilder.build(), 
				properties);
		
//		start our streams application  
		kafkaStreams.start();
	}
	
	private static Integer extractUserFolowerInTweet(String jsonTweet) {
		try {
			
			return JsonParser.parseString(jsonTweet)
				.getAsJsonObject()
				.get("user")
				.getAsJsonObject()
				.get("followers_count")
				.getAsInt();
		} catch (Exception e) {
			return 0;
		}
	}
	
}


